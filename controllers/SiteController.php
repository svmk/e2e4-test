<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\base\Model;
use app\models\ContentSearch;
use yii\data\ActiveDataProvider;
use yii\web\HttpException;
use app\parsers\ParserException;
use yii\base\InvalidConfigException;
class SiteController extends Controller
{    
    public function actions()
    {
        return [
            'verbs' => [
                'class' => \yii\filters\VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * actionIndex выводит страницу сбора информации
     * 
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new ContentSearch;
        $isSaved = false;
        if ($searchModel->load(Yii::$app->request->post())) {
            $transaction = Yii::$app->db->beginTransaction();
            try {
                $isSaved = $searchModel->save();
                $parser = $searchModel->getParser();
                $subModel = null;
                if ($parser) {
                    $subModel = $parser->getModel(true);
                }
                if ($subModel) {
                    if ($subModel->load(Yii::$app->request->post())) {                        
                        if ($isSaved) {                            
                            $isSaved = $subModel->save();
                        }
                    }
                }
                if ($isSaved) {
                    $transaction->commit();
                } else {
                    $transaction->rollback();    
                }             
            } catch (\RuntimeException $ex) {
                $transaction->rollback();
                throw $ex;
            }
        }
        $parserError = null;
        if ($isSaved) {
            $parser = $searchModel->getParser();
            if ($parser) {
                try {
                    $parser->parse();                    
                } catch (ParserException $ex) {
                    $parserError = $ex;
                }
            }
        }
        $dataProvider = new ActiveDataProvider([
            'query' => ContentSearch::find(),
        ]);
        return $this->render(
            'index',
            [
                'isSaved' => $isSaved,
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
                'parserError' => $parserError,
            ]
        );
    }

    /**
     * Выводит вторую форму для настройки
     * 
     * @return string|null
     */
    public function actionRenderSubForm() {
        $searchModel = new ContentSearch;
        $searchModel->load(Yii::$app->request->post());
        $parser = $searchModel->getParser();
        if ($parser) {
            $subModel = $parser->getModel(true);
            $subView  = $parser->getView('/content-search-subforms/%s/create-form');
            if (!empty($subModel) && !empty($subView)) {
                return $this->renderPartial(
                    'render-sub-form',
                    [
                        'subView' => $subView,
                        'subModel' => $subModel,
                    ]
                );
            }
        }
        return null;
    }

    /**
     * actionDelete удаляет запись
     * 
     * @param integer $id идентификатор записи
     *
     */
    public function actionDelete($id) {
        $model = ContentSearch::findOne($id);
        if ($model === null) {
            throw new HttpException(404,'Запись не найдена');
        }
        $model->delete();
        $this->redirect(['index']);
    }

    /**
     * actionView выводит результаты работы парсера
     * 
     * @param integer $id ID записи
     *
     * @return string
     */
    public function actionView($id) {
        $model = ContentSearch::findOne($id);
        if ($model === null) {
            throw new HttpException(404,'Запись не найдена');
        }
        $parser = $model->getParser();
        if ($parser === null) {
            throw new InvalidConfigException('Запись не сконфигурирована');            
        }
        $dataProvider = new ActiveDataProvider([
            'query' => $parser->resultQuery(),
        ]);
        $columns = $parser->resultColumns();
        $viewParams = [
            'model' => $model,
            'dataProvider' => $dataProvider,
            'columns' => $columns,
        ];
        if (Yii::$app->request->getIsAjax()) {
            return $this->renderPartial('view',$viewParams);
        } else {            
            return $this->render('view',$viewParams);
        }
    }
}
