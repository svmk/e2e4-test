<?php
use yii\bootstrap\ActiveForm;
use yii\widgets\Pjax;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\Json;
use yii\widgets\PjaxAsset;
use yii\grid\GridView;
use yii\grid\ActionColumn;
?>
<!-- секция формы начало -->
<?php Pjax::begin(['id' => 'create-search-section',]) ?>
    <?php if ($parserError) { ?>
        <div class="alert alert-danger" role="alert">
            <h4>Ошибка индексации страницы</h4>
            <p><?php echo Html::encode($parserError->getUserMessage()); ?></p>
            <p>
                <a href="<?php echo Url::to(['index']); ?>" id="create-search-form-new-btn">
                    Добавить другой сайт
                </a>
            </p>
        </div>
    <?php } elseif ($isSaved) { ?>
        <div class="alert alert-success" role="alert">
            <h4>Сайт успешно добавлен</h4>
            <p>
                <a href="<?php echo Url::to(['index']); ?>" id="create-search-form-new-btn">
                    Добавить еще один сайт
                </a>
                <a href="#list-search">Перейти к списку сайтов</a>
            </p>                
        </div>
    <?php } else {
        $searchForm = ActiveForm::begin([
            'id' => 'create-search-form',
            'options' => [
                'data-pjax' => true,
            ],
        ]);
        $config = $searchModel->getParser();
        $subModel = null;
        if ($config) {
            $subModel = $config->getModel(true);
            $subView  = $config->getView('/content-search-subforms/%s/create-form');
        }
        if ($subModel) {
            echo $searchForm->errorSummary([$searchModel,$subModel]);
        } else {
            echo $searchForm->errorSummary([$searchModel]);
        }
        ?>
        <?= $searchForm->field(
            $searchModel,'site'
        )->label(
            false     
        )->textInput([
            'placeholder' => 'Сайт',
            'required' => true,
        ]) ?>
        <?= $searchForm->field(
            $searchModel,'elementTypeFilter'
        )->label(
            false     
        )->radioList(
            $searchModel->getElementTypeTitles()
        ) ?>
        <div id="create-search-form-element-subform">
            <?php                
                if (!empty($subModel) && !empty($subView)) {
                    echo $this->render(
                        $subView,
                        [
                            'model' => $subModel,
                            'form' => $searchForm,
                        ]
                    );
                }
            ?>            
        </div>
        <div class="form-group">
            <?= Html::submitButton('Найти', ['class' => 'btn btn-primary']) ?>
        </div>
        <?php ActiveForm::end(); ?> 
    <?php } ?>
<?php Pjax::end(); ?>    
<!-- секция формы конец -->