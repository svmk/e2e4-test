<?php
/* @var $this yii\web\View */
/* @var $subView string */
/* @var $subModel yii\db\ActiveRecord */
use Yii;
use yii\bootstrap\ActiveForm;
use yii\helpers\Json;
use yii\helpers\Html;
$searchForm = Yii::createObject([
	'class' => ActiveForm::className(),
	'id' => 'create-search-form',
]);
echo $this->render($subView,['model' => $subModel, 'form' => $searchForm, ]);
$options    = Json::encode($searchForm->options);
foreach ($searchForm->attributes as $attribute) {
	$attribute = Json::encode($attribute);
	echo Html::script("jQuery('#$searchForm->id').yiiActiveForm('add',$attribute, $options);");
}
?>