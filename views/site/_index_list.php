<?php
use yii\bootstrap\ActiveForm;
use yii\widgets\Pjax;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\Json;
use yii\widgets\PjaxAsset;
use yii\grid\GridView;
use yii\grid\ActionColumn;
?>
<!-- секция списка начало -->
<?php Pjax::begin(['id' => 'list-search',]) ?>
<?php 
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'id',
            'site',
            [
                'attribute' => 'elementTypeFilter',
                'value' => function($model) {
                    $config = $model->getParser();
                    if ($config) {
                        return $config->getTitle();
                    }
                }
            ],
            [
                'class' => ActionColumn::className(),
                'header' => 'Управление',
                'template' => '{view} {delete}',
                'buttons' => [
                    'view' => function ($url, $model, $key) {
                        $options = [
                            'title' => Yii::t('yii', 'View'),
                            'aria-label' => Yii::t('yii', 'View'),
                            'data-pjax' => '0',
                            'class' => 'list-search-modal',
                            'data-url' => $url,
                        ];
                        return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, $options);
                    }
                ],
            ],
        ],
    ]);
?>
<?php Pjax::end(); ?>    
<!-- секция списка конец -->