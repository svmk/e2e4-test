<?php
/* @var $this yii\web\View */
/* @var $model app\models\ContentSearch */
use yii\grid\GridView;
?>
<div class="site-view">
	<?php 
		echo GridView::widget([
			'dataProvider' => $dataProvider,
			'columns' => $columns,
		]);
	?>
</div>