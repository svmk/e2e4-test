<?php
/* @var $this yii\web\View */
use yii\bootstrap\ActiveForm;
use yii\widgets\Pjax;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\Json;
use yii\widgets\PjaxAsset;
use yii\grid\GridView;
use yii\grid\ActionColumn;
use yii\bootstrap\Modal;
$this->title = 'Поиск контента на главной странице сайта';
?>
<div class="site-index">
    <?php 
        echo Modal::widget([
            'id' => 'result-viewer',
            'size' => Modal::SIZE_LARGE,
            'header' => 'Результаты работы парсера',
        ]);
    ?>    
    <div class="page-header">
        <h1 id="sec-1">Новый сайт</h1>
    </div>                
    <?php
        echo $this->render('_index_form',[
            'isSaved' => $isSaved,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'parserError' => $parserError,
        ]);
    ?>
    <div class="page-header">
        <h1 id="sec-2">Список сайтов</h1>
    </div>                
    <?php
        echo $this->render('_index_list',[
            'isSaved' => $isSaved,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'parserError' => $parserError,
        ]);
    ?>
</div>
<?php
PjaxAsset::register($this);
$subFormLoader = Json::encode(Url::to(['render-sub-form']));
$this->registerJs(
    "$(document).delegate(
        '#create-search-form [name=\"ContentSearch[elementTypeFilter]\"]',
        'change',
        function(){
            var data = $('#create-search-form').serialize();
            $.ajax({
                url: $subFormLoader,
                type:'POST',
                data: data,
                success: function(content) {
                    // Добавляем скрипты для валидации.
                    var dom = $(content);
                    dom.filter('script').each(function(){
                        var script = this.text || this.textContent || this.innerHTML || '';
                        $.globalEval(script);
                    });
                    $('#create-search-form-element-subform').html(content);
                }        
            });
        }
    );"
);
$this->registerJs("
    $('#create-search-form button[type=\"submit\"]').attr('disabled',true);
    $('#create-search-form').on('afterValidateAttribute',function(event,attribute,messages){        
        var isValid = $('#create-search-form .has-error').length == 0;
        isValid = isValid && $('#create-search-form .has-success').length > 0;
        $(
            '#create-search-form button[type=\"submit\"]'
        ).attr(
            'disabled',
            !isValid
        );        
    });
    $(document).on('pjax:success', '#create-search-section', function(event) {
        $.pjax.reload('#list-search');
        return true;
    });    
    $(document).delegate(
        '.list-search-modal',
        'click', 
        function(event) {
            var url = $(this).data('url');
            $('#result-viewer .modal-body').load(url);
            event.preventDefault();
            $('#result-viewer').modal();
        }
    );
");
?>