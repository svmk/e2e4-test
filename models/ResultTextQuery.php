<?php
namespace app\models;
use yii\db\ActiveQuery;

class ResultTextQuery extends ActiveQuery {
    /**
     * thatHaveSearch фильтрует по поиску
     * 
     * @param ContentSearch|integer $model модель
     *
     * @return this
     */
	public function thatHaveSearch($model) {
		if ($model instanceof ContentSearch) {
			$model = $model->getPrimaryKey();
		}
		$this->andWhere(['content_search_id' => $model,]);
		return $this;
	}
}