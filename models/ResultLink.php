<?php
namespace app\models;
use yii\db\ActiveRecord;
use Yii;
/**
* ResultLink результаты парсинга
* @uses     ActiveRecord
*/
class ResultLink extends ActiveRecord {
	/**
     * @inheritdoc
     */
	public static function tableName()
    {
    	return 'content_result_links';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['url','content_search_id'],'required'],
            [['url',],'url'],      
            [['text'],'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'url' => 'URL',
            'text' => 'Текст ссылки',
        ];
    }

    /**
     * @inheritdoc
     */
    public static function find()
    {
        return Yii::createObject(ResultLinkQuery::className(), [get_called_class()]);        
    }

    /**
     * setSearch устанавливает поиск
     * 
     * @param ContentSearch $model модель
     *
     */
    public function setSearch($model) {
        if ($model instanceof ContentSearch) {
            $model = $model->getPrimaryKey();
        }
        $this->content_search_id = $model;
    }
}