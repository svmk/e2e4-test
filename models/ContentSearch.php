<?php
namespace app\models;
use Yii;
use app\parsers\ContentSearchTextParser;
use app\parsers\ContentSearchImageParser;
use app\parsers\ContentSearchLinkParser;
use yii\helpers\ArrayHelper;
use yii\db\ActiveRecord;

/**
* ContentSearch
* @uses     ActiveRecord
*/
class ContentSearch extends ActiveRecord {

	const CONTENT_TYPE_TEXT = 1;
	const CONTENT_TYPE_IMAGE = 2;
	const CONTENT_TYPE_LINK  = 3;

    /**
     * $site сайт на котором ищем информацию
     * @var string
     */

    /**
     * $elementTypeFilter фильтрация по типу элемента
     * @var string
     */

    /**
     * @inheritdoc
     */
	public static function tableName()
    {
    	return 'content_search';
    }

    /**
     * @inheritdoc
     */
	public function attributeLabels()
    {
    	return [
    		'site' => 'Сайт',
    		'elementTypeFilter' => 'Тип данных',
    	];
    }

    /**
     * @inheritdoc
     */
    public function rules() {
    	return [
    		[['site','elementTypeFilter'],'required'],
    		[['site',],'string'],
    		[['site',],'match','pattern' => '/^(?:[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?\.)+[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?$/',], // Взято из EmailValidator
    		[['elementTypeFilter',],'number'],
    	];
    }

    /**
     * getElementTypeTitles возвращает типы элементов
     * 
     * @return array
     */
    public function getElementTypeTitles() {
 		return ArrayHelper::map(
 			$this->subForms(),
 			function($model){
 				return $model->getId();
 			},
 			function($model){
 				return $model->getTitle();
 			}
		);
    }

    /**
     * getParser возвращает парсер
     * 
     * @return AbstractContentSearchParser|null
     */
    public function getParser() {
    	$result = null;
    	foreach ($this->subForms() as $config) {
    		if ($config->getId() == $this->elementTypeFilter) {
    			$result = $config;
    			break;
    		}
    	}
    	return $result;
    }

    /**
     * subForms возвращает подформы
     * 
     * @return ContentSearchSubFormHelper[]
     */
    protected function subForms() {
    	return Yii::$app->parser->getParsers($this);
    }
}