<?php
namespace app\models;
use yii\db\ActiveRecord;
use Yii;
class ContentSearchText extends ActiveRecord {
	/**
     * $query запрос на поиск информации
     * @var string
     */
    
    /**
     * @inheritdoc
     */
	public static function tableName()
    {
    	return 'content_search_text';
    }

    /**
     * @inheritdoc
     */
	public function attributeLabels()
    {
    	return [
    		'query' => 'Запрос',
    	];
    }

    /**
     * @inheritdoc
     */
    public function rules() {
    	return [
    		[['query','content_search_id'],'required'],
    		[['query',],'string'],    	
            [['content_search_id'],'integer'],
    	];
    }

    /**
     * @inheritdoc
     */
    public static function find()
    {
        return Yii::createObject(ContentSearchTextQuery::className(), [get_called_class()]);        
    }

    /**
     * setSearch устанавливает поиск
     * 
     * @param ContentSearch $model модель
     *
     */
    public function setSearch($model) {
        if ($model instanceof ContentSearch) {
            $model = $model->getPrimaryKey();
        }
        $this->content_search_id = $model;
    }

}