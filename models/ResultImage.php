<?php
namespace app\models;
use yii\db\ActiveRecord;
use Yii;
/**
* ResultLink результаты парсинга
* @uses     ActiveRecord
*/
class ResultImage extends ActiveRecord {
	/**
     * @inheritdoc
     */
	public static function tableName()
    {
    	return 'content_result_images';
    }

    /**
     * @inheritdoc
     */
    public static function find()
    {
        return Yii::createObject(ResultImageQuery::className(), [get_called_class()]);        
    }

    /**
     * setSearch устанавливает поиск
     * 
     * @param ContentSearch $model модель
     *
     */
    public function setSearch($model) {
        if ($model instanceof ContentSearch) {
            $model = $model->getPrimaryKey();
        }
        $this->content_search_id = $model;
    }
}