<?php
namespace app\parsers;
use app\models\ContentSearch;
use app\models\ResultLink;
use Yii;
use Purl\Url;
class ContentSearchLinkParser extends AbstractContentSearchParser {

    /**
     * resultQuery возвращает запрос к БД
     * @return yii\db\ActiveQuery
     */
    public function resultQuery() {
        return ResultLink::find()->thatHaveSearch($this->getSearchModel());
    }

    /**
     * resultColumns возвращает столбцы результата
     * 
     * @return array
     */
    public function resultColumns() {
        return [
            [
                'attribute' => 'url',
                'format' => 'url',
            ],
            'text',
        ];
    }

	/**
     * getLitera возвращает литеру
     * 
     * @return string
     */
    public function getLitera() {
    	return 'link';
    }

	/**
     * getId возвращает ID подформы
     * 
     * @return integer
     */
	public function getId() {
		return ContentSearch::CONTENT_TYPE_LINK;
	}

    /**
     * getTitle возвращает текст формы
     * 
     * @return string
     */
	public function getTitle() {
		return 'Ссылки';
	}

    /**
     * @inheritdoc
     */
    public static function findConfigModel(ContentSearch $model) {
 		return null;
    }
   
    /**
     * @inheritdoc
     */
    public static function createConfigModel(ContentSearch $model) {
    	return null;
    }

    /**
     * @inheritdoc
     */
    public function parse() {
        $searchModel = $this->getSearchModel();
        $webPageUrl = new Url('http://'.$searchModel->site.'/');
        $document = Yii::$app->parser->crawl(
            $webPageUrl
        );
        $links = [];
        foreach ($document->filter('a') as $link) {
            $href = $link->getAttribute('href');
            if ($href) {
                try {
                    $url = $webPageUrl->join($href);
                    $links[] = [(string) $url, $link->textContent];
                } catch (\Exception $ex) {

                }
            }
        }
        foreach ($links as list($link,$text)) {
            $resultItem = new ResultLink;
            $resultItem->setSearch($searchModel);
            $resultItem->url = $link;
            $resultItem->text = $text;
            $resultItem->save();
        }
    }
}