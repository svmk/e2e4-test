<?php
namespace app\parsers;
use app\models\ContentSearch;
use app\models\ContentSearchText;
use app\models\ResultText;
use Yii;
class ContentSearchTextParser extends AbstractContentSearchParser {

    /**
     * resultQuery возвращает запрос к БД
     * @return yii\db\ActiveQuery
     */
    public function resultQuery() {
        return ResultText::find()->thatHaveSearch($this->getSearchModel());
    }

    /**
     * resultColumns возвращает столбцы результата
     * 
     * @return array
     */
    public function resultColumns() {
        return [
            'text',
        ];
    }
    
	/**
     * getLitera возвращает литеру
     * 
     * @return string
     */
    public function getLitera() {
    	return 'text';
    }

	/**
     * getId возвращает ID подформы
     * 
     * @return integer
     */
	public function getId() {
		return ContentSearch::CONTENT_TYPE_TEXT;
	}

    /**
     * getTitle возвращает текст формы
     * 
     * @return string
     */
	public function getTitle() {
		return 'Текст';
	}

    /**
     * @inheritdoc
     */
    public static function findConfigModel(ContentSearch $model) {
 		return ContentSearchText::find()->thatHaveSearch($model)->one();   	
    }
   
    /**
     * @inheritdoc
     */
    public static function createConfigModel(ContentSearch $model) {
    	$result = new ContentSearchText;
    	$result->setSearch($model);
    	return $result;
    }

    /**
     * @inheritdoc
     */
    public function parse() {
        $searchModel = $this->getSearchModel();
        $webPageUrl = 'http://'.$searchModel->site.'/';
        $document = Yii::$app->parser->crawl(
            $webPageUrl
        );
        $result = [];
        foreach ($document->filter($this->getModel()->query) as $element) {
            $text = trim($element->textContent);
            if (!empty($text)) {                
                $result[] = $text;
            }
        }
        foreach ($result as $text) {
            $resultItem = new ResultText;
            $resultItem->setSearch($searchModel);            
            $resultItem->text = $text;
            $resultItem->save();
        }
    }
}