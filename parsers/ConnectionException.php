<?php
namespace app\parsers;
use GuzzleHttp\Exception\GuzzleException;
class ConnectionException extends \RuntimeException implements ParserException {
    /**
     * $exception 
     * @var GuzzleException
     */
    protected $exception;

	/**
     * getParentException возвращает родительское исключение
     * 
     * @return Exception
     */
	public function getParentException() {
		return $this->exception;
	}

    /**
     * getUserMessage возвращает сообщение для пользователя
     * 
     * @return string
     */
	public function getUserMessage() {
        $exception = $this->exception;
        if ($exception) {            
    		return 'Ошибка подключения: '.$exception->getMessage();
        } else {
            return 'Ошибка подключения';
        }
	}

    /**
     * __construct создаёт исключение
     * 
     * @param GuzzleException $exception исключение
     *
     */
    function __construct(GuzzleException $exception) {
        $this->exception = $exception;
    }
}