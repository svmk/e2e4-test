<?php
namespace app\parsers;
use app\models\ContentSearch;
abstract class AbstractContentSearchParser {

    /**
     * $model модель поиска
     * @var ContentSearch
     */
	private $model;

    /**
     * resultQuery возвращает запрос к БД
     * @return yii\db\ActiveQuery
     */
    abstract public function resultQuery();

    /**
     * resultColumns возвращает столбцы результата
     * 
     * @return array
     */
    abstract public function resultColumns();

    /**
     * getLitera возвращает литеру
     * 
     * @return string
     */
    abstract public function getLitera();

    /**
     * getId возвращает ID подформы
     * 
     * @return integer
     */
	abstract public function getId();

    /**
     * getTitle возвращает текст формы
     * 
     * @return string
     */
	abstract public function getTitle();

    /**
     * findConfigModel ищет модель
     * @param  ContentSearch $model модель
     * @return ActiveRecord
     */
    abstract public static function findConfigModel(ContentSearch $model);

    /**
     * createConfigModel создаёт модель
     * @param  ContentSearch $model модель
     * @return ActiveRecord
     */
    abstract public static function createConfigModel(ContentSearch $model);

    /**
     * parse парсит данные
     */
    abstract public function parse();

    /**
     * getModel возвращает модель
     * getModel
     * @param  boolean $force принудительно создавать модель
     * @return yii\db\ActiveRecord|null
     */
	public function getModel($force = false) {
		$result = null;
		if ($this->model) {
			if (!$this->model->getIsNewRecord() || $force) {				
				$result = static::findConfigModel($this->model);
				if ($result === null) {
					$result = static::createConfigModel($this->model);
				}
			}
		}
		return $result;
	}

    /**
     * getView возвращает путь до представления
     * 
     * @param string $template шаблон
     *
     * @return string|null
     */
	public function getView($template) {
		$result = null;
		if (!empty($this->model) && !empty($this->getLitera())) {
			return sprintf($template,$this->getLitera());
		}
		return $result;
	}

    /**
     * getSearchModel возвращает модель поиска
     * 
     * @return ContentSearch
     */
    protected function getSearchModel() {
        return $this->model;
    }

    /**
     * init инициализирует парсер
     */
    protected function init() {

    }

    /**
     * __construct приватный конструктор
     * @param  ContentSearch $model модель
     * @param  array $params настройки
     */
    function __construct(ContentSearch $model,$params = []) {
        $this->model = $model;
        foreach ($params as $key => $value) {
            $this->{$key} = $value;
        }
        $this->init();
	}

}