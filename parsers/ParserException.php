<?php
namespace app\parsers;
interface ParserException {
    /**
     * getParentException возвращает родительское исключение
     * 
     * @return Exception
     */
	public function getParentException();

    /**
     * getUserMessage возвращает сообщение для пользователя
     * 
     * @return string
     */
	public function getUserMessage();
}