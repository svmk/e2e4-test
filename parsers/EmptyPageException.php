<?php
namespace app\parsers;
class EmptyPageException extends \RuntimeException implements ParserException {
	/**
     * getParentException возвращает родительское исключение
     * 
     * @return Exception
     */
	public function getParentException() {
		return null;
	}

    /**
     * getUserMessage возвращает сообщение для пользователя
     * 
     * @return string
     */
	public function getUserMessage() {
		return 'Сервер вернул пустую страницу без HTML-кода';
	}

    /**
     * @inheritdoc
     */
	public function getMessage() {
		return $this->getUserMessage();
	}
}