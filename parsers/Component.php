<?php
namespace app\parsers;
use app\models\ContentSearch;
use Symfony\Component\DomCrawler\Crawler;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
class Component extends \yii\base\Component {
    /**
     * $parsers парсеры
     * @var array
     */
	public $parsers = [];

    /**
     * $guzzleHttpConfig настройки для GuzzleHTTP
     * @var array
     */
	public $guzzleHttpConfig = [];

    /**
     * getParsers возвращает парсеры
     * 
     * @param ContentSearch $model модель
     * @return AbstractContentSearchParser[]
     */
	public function getParsers(ContentSearch $model) {
		$result = [];
		foreach ($this->parsers as $className => $config) {
			if (is_string($config)) {
				$className = $config;
			}
			if (!is_array($config)) {
				$config = [];
			}
			$parser = new $className($model,$config);
			$result[] = $parser;
		}
		return $result;
	}

    /**
     * getGuzzleHttpClient возвращает веб-клиент
     * 
     * @return GuzzleHttp\Client
     */
	public function getGuzzleHttpClient() {
		return new Client($this->guzzleHttpConfig);
	}

    /**
     * crawl скачивает и парсит страницу при помощи GET запроса
     * 
     * @param string $url URL
     *
     * @throws ParserException
     * @return Crawler
     */
	public function crawl($url) {
		$result = null;
		try {
			$client = $this->getGuzzleHttpClient();
			$response = $client->request('GET',$url);
			$html = (string) $response->getBody();
			if ($html) {
				$result = new Crawler($html,$url);			
			} else {
				throw new EmptyPageException;
			}
		} catch (GuzzleException $ex) {
			throw new ConnectionException($ex);
		}
		return $result;
	}
}