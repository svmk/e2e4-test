<?php
namespace app\parsers;
use app\models\ContentSearch;
use app\models\ResultImage;
use Yii;
use Purl\Url;
use yii\base\InvalidConfigException;
use yii\grid\Column;
use yii\helpers\Html;
class ContentSearchImageParser extends AbstractContentSearchParser {

    /**
     * $maxImageWidth максимальная ширина изображения в виджете
     * @var integer|float
     */
    public $maxImageWidth = 150.0;

    /**
     * $maxImageHeight максимальная высота изображения в виджете
     * @var integer|float
     */
    public $maxImageHeight = 100.0;

    /**
     * $storagePath путь до хранилища
     * @var string
     */
    public $storagePath;

    /**
     * $storageUrl путь до хранилища
     * @var string
     */
    public $storageUrl;

    /**
     * resultQuery возвращает запрос к БД
     * @return yii\db\ActiveQuery
     */
    public function resultQuery() {
        return ResultImage::find()->thatHaveSearch($this->getSearchModel());
    }

    /**
     * resultColumns возвращает столбцы результата
     * 
     * @return array
     */
    public function resultColumns() {
        $self = $this;        
        return [
            [
                'class' => Column::className(),
                'content' => function ($model, $key, $index, $column) use($self) {
                    $imageSize = $self->getImageSize($model);
                    if ($imageSize) {
                        list($width,$height) = $imageSize;
                        $url = $self->getFileUrl($model);
                        return Html::a(                            
                            Html::img(
                                $url,
                                [
                                    'width' => $width, 
                                    'height' => $height,
                                ]
                            ),
                            $url,
                            [
                                'target' => '_blank',
                            ]
                        );                        
                    }
                }
            ],
        ];
    }

    /**
     * getImageSize возвращает размер изображения
     * 
     * @param ResultImage $model модель.
     *
     * @return null|[$width,$height]
     */
    protected function getImageSize($model) {
        $maxImageWidth = (float) $this->maxImageWidth;
        $maxImageHeight = (float) $this->maxImageHeight;
        $path = $this->getFilePath($model);
        $imageSize = @getimagesize($path);
        if ($imageSize) {
            $width = $imageSize[0];
            $height = $imageSize[1];
            $ratio = 1.0;
            if ($width / $maxImageWidth >= $ratio) {
                $ratio = $width / $maxImageWidth;
            }
            if ($height / $maxImageHeight >= $ratio) {
                $ratio = $height / $maxImageHeight;
            }
            $width = floor($width / $ratio);
            $height = floor($height / $ratio);
            return [$width,$height];
        }
        return null;
    }

	/**
     * getLitera возвращает литеру
     * 
     * @return string
     */
    public function getLitera() {
    	return 'image';
    }

	/**
     * getId возвращает ID подформы
     * 
     * @return integer
     */
	public function getId() {
		return ContentSearch::CONTENT_TYPE_IMAGE;
	}

    /**
     * getTitle возвращает текст формы
     * 
     * @return string
     */
	public function getTitle() {
		return 'Картинки';
	}

    /**
     * @inheritdoc
     */
    public static function findConfigModel(ContentSearch $model) {
 		return null;
    }
   
    /**
     * @inheritdoc
     */
    public static function createConfigModel(ContentSearch $model) {
    	return null;
    }

    /**
     * @inheritdoc
     */
    public function parse() {
        $searchModel = $this->getSearchModel();
        $webPageUrl = new Url('http://'.$searchModel->site.'/');
        $document = Yii::$app->parser->crawl(
            $webPageUrl
        );
        $images = [];
        foreach ($document->filter('img') as $link) {
            $href = $link->getAttribute('src');
            if ($href) {
                try {
                    $url = $webPageUrl->join($href);
                    $images[] = (string) $url;
                } catch (\Exception $ex) {

                }
            }
        }
        foreach ($images as $url) {
            $extension = null;
            if (substr($url,0,5) == 'data:') {
                $fp   = fopen($url, 'r');
                if ($fp) {
                    $meta = stream_get_meta_data($fp);
                    if (!empty($meta['mediatype'])) {
                        if ($meta['mediatype'] == 'image/jpegp') {
                            $extension = 'jpeg';
                        } elseif ($meta['mediatype'] == 'image/jpeg') {
                            $extension = 'jpeg';
                        } elseif ($meta['mediatype'] == 'image/png') {
                            $extension = 'png';
                        } elseif ($meta['mediatype'] == 'image/gif') {
                            $extension = 'gif';
                        }
                    }                    
                }
            }
            $url = new Url($url);
            $urlPath = $url->getPath();
            if ($urlPath) {
                $extension = pathinfo($urlPath,PATHINFO_EXTENSION);            
            }
            if ($extension === null) {
                $extension = 'dat';
            }
            $path = $searchModel->site.'/'.md5($url).'.'.$extension;
            $dstFile = $this->getFilePath(
                $path
            );
            $dstDir = dirname($dstFile);
            if (!is_dir($dstDir)) {
                mkdir($dstDir,0777,true);
            }
            copy($url,$dstFile);
            $resultItem = new ResultImage;
            $resultItem->setSearch($searchModel);
            $resultItem->url = $url;
            $resultItem->path = $path;
            $resultItem->save();
        }
    }

    /**
     * getFilePath возвращает путь до файла
     * 
     * @param string|ResultImage $path путь до файла
     *
     * @return string
     */
    protected function getFilePath($path) {
        if ($path instanceof ResultImage) {
            $path = $path->path;
        }
        return Yii::getAlias(
            rtrim($this->storagePath,'/').'/'.$path
        );
    }

    /**
     * getFileUrl возвращает URL файла
     * 
     * @param string|ResultImage $path путь до файла
     *
     * @return string
     */
    protected function getFileUrl($path) {
        if ($path instanceof ResultImage) {
            $path = $path->path;
        }
        return Yii::getAlias(
            rtrim($this->storageUrl,'/').'/'.$path
        );
    }

    /**
     * @inheritdoc
     */
    protected function init() {
        if ($this->storagePath === null) {
            throw new InvalidConfigException('Не заполнен параметр storagePath');
        }
        if ($this->storageUrl === null) {
            throw new InvalidConfigException('Не заполнен параметр storageUrl');
        }
    }
}