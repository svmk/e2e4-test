<?php

use yii\db\Migration;

/**
 * Handles the creation for table `content_search`.
 */
class m160609_043523_create_content_search extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('content_search', [
            'id' => $this->primaryKey(),
            'site' => $this->string(),
            'elementTypeFilter' => $this->integer(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('content_search');
    }
}
