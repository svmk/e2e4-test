<?php

use yii\db\Migration;

/**
 * Handles the creation for table `content_result_texts`.
 */
class m160609_122451_create_content_result_texts extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('content_result_texts', [
            'id' => $this->primaryKey(),
            'content_search_id' => $this->integer(),
            'text' => $this->text(),
        ]);
        $this->createIndex(
            'content_result_texts_content_search_id',
            'content_result_texts',
            'content_search_id'
        );
        $this->addForeignKey(
            'content_result_texts_content_search_id',
            'content_result_texts',
            ['content_search_id'],
            'content_search',
            ['id'],
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('content_result_texts');
    }
}
