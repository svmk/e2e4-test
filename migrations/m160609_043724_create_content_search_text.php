<?php

use yii\db\Migration;

/**
 * Handles the creation for table `content_search_text`.
 */
class m160609_043724_create_content_search_text extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('content_search_text', [
            'id' => $this->primaryKey(),
            'content_search_id' => $this->integer(),
            'query' => $this->text(),
        ]);
        $this->createIndex(
            'content_search_text_content_search_id',
            'content_search_text',
            'content_search_id',
            true
        );
        $this->addForeignKey(
            'content_search_text_content_search_id',
            'content_search_text',
            ['content_search_id'],
            'content_search',
            ['id'],
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('content_search_text');
    }
}
