<?php

use yii\db\Migration;

/**
 * Handles the creation for table `content_result_links`.
 */
class m160609_122816_create_content_result_links extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('content_result_links', [
            'id' => $this->primaryKey(),
            'content_search_id' => $this->integer(),
            'url' => $this->text(),
            'text' => $this->text(),
        ]);
        $this->createIndex(
            'content_result_links_content_search_id',
            'content_result_links',
            'content_search_id'
        );
        $this->addForeignKey(
            'content_result_links_content_search_id',
            'content_result_links',
            ['content_search_id'],
            'content_search',
            ['id'],
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('content_result_links');
    }
}
