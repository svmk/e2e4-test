<?php

return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=localhost;dbname=e2e4-test',
    'username' => 'e2e4-test',
    'password' => 'e2e4-test',
    'charset' => 'utf8',
];
