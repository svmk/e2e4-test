<?php
return [
	'class' => 'app\\parsers\\Component',
	'parsers' => [
		'app\\parsers\\ContentSearchLinkParser',
		'app\\parsers\\ContentSearchTextParser',
		'app\\parsers\\ContentSearchImageParser' => [
			'storagePath' => '@webroot/storage',
			'storageUrl'  => 'storage',
		],
	],
];