Тестовое задание e2e4
=====================

Тестовое задание выполнено на базе [Yii-фреймворка](http://www.yiiframework.com/) второй версии.

Структура каталогов
-------------------
      config/parser.php                       содержит настройки парсеров
      controllers/                            содержит контроллеры
      migrations/                             содержит миграции
      models/                                 содержит модели
      parsers/                                содержит парсеры и исключения
      tests/codeception/unit/ParserTest.php   тест парсеров      
      views/                                  содержит представления
      web/storage                             хранилище картинок



Требования перед установкой
---------------------------

* PHP 5.4
* Расширение php-gd


Установка
---------

### Установите зависимости используя composer

Если у вас нет [Composer](http://getcomposer.org/), вы должны установить его [getcomposer.org](http://getcomposer.org/doc/00-intro.md#installation-nix).

После чего выполните команды из корневого каталога проекта

~~~
php composer.phar global require "fxp/composer-asset-plugin:~1.1.1"
php composer.phar install --no-dev
~~~

### Настройте подключение к БД

Отредактируйте файл `config/db.php` с настоящими настройками.

```php
return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=localhost;dbname=e2e4-test',
    'username' => 'e2e4-test',
    'password' => 'e2e4-test',
    'charset' => 'utf8',
];
```

### Выполните миграции

Из каталога проекта выполните команду
```
./yii migrate/up
```

После выполнения всех шагов можете открыть страницу


Возможные проблемы
------------------

Есть возможность конфликта прав доступа к каталогу
```web/storage```

Архитектура проекта
-------------------

Архитектура проекта разрабатывалась с целью возможности расширения функций веб-приложения без изменения кода. (Или с минимальными изменениями)

Для этого мы на каждый парсер создали по классу, и наследовали их от ```app\parsers\AbstractContentSearchParser```


Список парсеров хранится в ```app\parsers\Component```
