<?php

use app\models\ContentSearch;
use app\models\ContentSearchText;
use app\models\ResultText;
use app\models\ResultLink;
use app\models\ResultImage;
class ParserTest extends \PHPUnit_Framework_TestCase
{
    protected function setUp()
    {
        $items = ContentSearch::find()->each();
        foreach ($items as $item) {
            $item->delete();
        }
    }

    protected function tearDown()
    {
    }

    public function testParseText()
    {
        $contentSearch = new ContentSearch;
        $contentSearch->site = 'lenta.ru';
        $contentSearch->elementTypeFilter = ContentSearch::CONTENT_TYPE_TEXT;
        $this->assertTrue($contentSearch->save());
        $contentSearchText = new ContentSearchText;
        $contentSearchText->setSearch($contentSearch);
        $contentSearchText->query = '.announce';
        $this->assertTrue($contentSearchText->save());
        $parser = $contentSearch->getParser();
        $this->assertNotNull($parser);
        $parser->parse();
        $this->assertTrue(ResultText::find()->count() > 0);
    }

    public function testParseLinks()
    {
        $contentSearch = new ContentSearch;
        $contentSearch->site = 'lenta.ru';
        $contentSearch->elementTypeFilter = ContentSearch::CONTENT_TYPE_LINK;
        $this->assertTrue($contentSearch->save());        
        $parser = $contentSearch->getParser();
        $this->assertNotNull($parser);
        $parser->parse();
        $this->assertTrue(ResultLink::find()->count() > 0);
    }

    public function testParseImages()
    {
        $contentSearch = new ContentSearch;
        $contentSearch->site = 'lenta.ru';
        $contentSearch->elementTypeFilter = ContentSearch::CONTENT_TYPE_IMAGE;
        $this->assertTrue($contentSearch->save());        
        $parser = $contentSearch->getParser();
        $this->assertNotNull($parser);
        $parser->parse();
        $this->assertTrue(ResultImage::find()->count() > 0);
    }
}
